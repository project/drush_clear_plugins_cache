<?php

namespace Drupal\drush_clear_plugins_cache\Commands;

use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class PluginManagerCacheCommands extends DrushCommands {

  /**
   * The Drupal container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * Constructs a new PluginManagerCacheCommands object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal container.
   */
  public function __construct(ContainerInterface $container) {
    parent::__construct();
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  /**
   * Clear plugin manager cached definitions.
   *
   * @param string $pluginType
   *   The plugin manager service name. Default 'all'.
   *
   * @command drush_clear_plugins_cache:clear-cache
   * @aliases pmcc
   * @usage drush_clear_plugins_cache:clear-cache plugin.manager.group_content_enabler
   *   Clear group_content_enabler plugin manager cache.
   * @usage drush_clear_plugins_cache:clear-cache all
   *   Clear all plugin manager cache.
   * @usage drush_clear_plugins_cache:clear-cache
   *   Clear all plugin manager cache.
   */
  public function clearCache($pluginType = 'all') {
    if ($pluginType == 'all') {
      $pluginManagersIds = $this->getPluginManagers();
      foreach ($pluginManagersIds as $pluginManagersId) {
        $this->clearPluginManagerCache($pluginManagersId);
      }
    }
    else {
      $this->clearPluginManagerCache($pluginType);
    }

  }

  /**
   * Get all plugin managers IDs.
   *
   * @return array|false
   *   The plugin managers list.
   */
  protected function getPluginManagers() {
    /** @var \Drupal\Component\DependencyInjection\Container $container */
    $container = $this->container;
    $serviceIds = $container->getServiceIds();
    $serviceIds = preg_grep("/plugin\.manager/", $serviceIds);
    return $serviceIds;
  }

  /**
   * Clear plugin manager definitions cache.
   *
   * @param string $service
   *   Plugin manager ID.
   */
  protected function clearPluginManagerCache($service) {
    // This stop execution if the service is wrong.
    $pluginManager = \Drupal::service($service);

    // Group module compatibility.
    if (get_class($pluginManager) == 'Drupal\group\Plugin\GroupContentEnablerManager') {
      $pluginManager->clearCachedGroupTypeCollections();
      $pluginManager->clearCachedPluginMaps();
      $this->logger()->success('Cleared group cache.');
    }

    if (method_exists($pluginManager, 'clearCachedDefinitions')) {
      $pluginManager->clearCachedDefinitions();
      $this->logger()->success("Cache for '$service' cleared.");
    }
    else {
      $this->logger()->warning("Cache for '$service' not cleared.");
    }
  }

}
